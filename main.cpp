﻿#include <iostream>
#include <Windows.h> // Für die ZeichneScheibe-Funktion

using namespace std;

void ZeichneScheibe(short n, short scheibe, short saeule, short pos) {
    COORD yx;
    yx.X = static_cast<SHORT>(1 + 2 * n * saeule - n - scheibe);
    yx.Y = static_cast<SHORT>(n - pos);
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), yx);
    for (int i = 0; i < 2 * scheibe - 1; i++) {
        cout << '\xdb';
    }
    Sleep(5); // Warten, um die Bewegung sichtbar zu machen
}

void LoescheScheibe(short n, short scheibe, short saeule, short pos) {
    COORD yx;
    yx.X = static_cast<SHORT>(1 + 2 * n * saeule - n - scheibe);
    yx.Y = static_cast<SHORT>(n - pos);
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), yx);
    for (int i = 0; i < 2 * scheibe - 1; i++) {
        cout << ' '; // Lösche die Scheibe mit Leerzeichen
    }
    Sleep(5); // Warten, um die Bewegung sichtbar zu machen
}

int Hanoi(int n, int s, int z, int m) {
    int bewegungen = 0; // Zähler für die Scheibenbewegungen

    if (n > 1) {
        bewegungen += Hanoi(n - 1, s, 6 - s - z, m);
    }

    // Bewege die Scheibe und zeichne sie
    cout << "Trage Scheibe " << n << " von Saeule " << s
         << " nach Saeule " << z << endl;
    bewegungen++;
    LoescheScheibe(n, n, s, m);
    ZeichneScheibe(n, n, z, m);

    if (n > 1) {
        bewegungen += Hanoi(n - 1, 6 - s - z, z, m - 1);
    }

    return bewegungen; // Rückgabe der Anzahl der Scheibenbewegungen
}

int main() {
    int scheiben = 4;
    int startSaeule = 1;
    int zielSaeule = 3;
    int startPosition = scheiben - 1; // Die unterste Position ist n - 1

    // Initialisiere die Konsole
    COORD coord;
    coord.X = 80;
    coord.Y = 25;
    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    int bewegungen = Hanoi(scheiben, startSaeule, zielSaeule, startPosition);

    cout << "Anzahl der Scheibenbewegungen: " << bewegungen << endl;

    return 0;
}